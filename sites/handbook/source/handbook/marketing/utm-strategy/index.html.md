---
layout: handbook-page-toc
title: UTM Strategy
description: Everything you need to know about our UTM strategy, which enables insights through the connected/resulting Sisense dashboards.
twitter_image: /images/tweets/handbook-marketing.png
twitter_site: '@gitlab'
twitter_creator: '@gitlab'
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
{: #overview .gitlab-purple}
<!-- DO NOT CHANGE ANCHOR -->
The purpose of this handbook page is to outline how our UTM strategy drives insightful dashboards in Sisense. You may learn how to use the UTM builder, and why it is critical that we use this consistent process across all marketing channels for consistent and useful reporting.

Everyone can contribute - See something you'd like to discuss or iterate on? Start an MR and post it in [#campaign-reporting](https://gitlab.slack.com/archives/CSBDEH2DQ) slack channel.

## UTMs and Sisense dashboards
{: #utms-sisense .gitlab-purple}
<!-- DO NOT CHANGE ANCHOR -->

Add notes here

## UTM link builder process
{: #utm-builder .gitlab-purple}
<!-- DO NOT CHANGE ANCHOR -->

Add notes here

## Marketo > Bizible integration
{: #marketo-bizible .gitlab-purple}
<!-- DO NOT CHANGE ANCHOR -->

Add notes here
