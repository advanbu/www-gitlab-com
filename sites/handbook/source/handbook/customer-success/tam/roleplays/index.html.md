---
layout: handbook-page-toc
title: "Roleplay Scenarios"
description: "This handbook page collects links to all roleplaying scenarios, for TAMs to utilize to improve their conversations and enable them to be audible-ready."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

### Roleplay Scenarios

- [Success planning](https://about.gitlab.com/handbook/customer-success/tam/success-plans/) and uncovering strategic goals
   - "Ticklish Licorice" scenario
      - [Customer info sheet](https://docs.google.com/document/d/19YWLSP6Kol5WwP83I01CSnbCwvdMUa6LXsiihOTNItI/edit#heading=h.qiwberg2gd4s)
      - [TAM info sheet](https://docs.google.com/document/d/1Jmkmz3cw4sFQdwfAw-hMd4HnhIjxW5pEumKstq0laiI/edit)
      - This scenario can be done 1:1 where the person acting as the customer can choose to be George or Alicia, but this scenario can be extra impactful in a group setting with multiple people acting as different customer personas.
   - "Green Grass Medical" scenario
      - [Background info](https://gitlab.edcast.com/pathways/tam-building-success-plans/cards/922704)
      - This roleplay is part of the "TAM Building Success Plans" EdCast course
- [Stage expansion](https://about.gitlab.com/handbook/customer-success/tam/stage-enablement-and-expansion/)
   - TODO build scenario and roles
- Booking an [EBR](https://about.gitlab.com/handbook/customer-success/tam/ebr/)
- [Onboarding](https://about.gitlab.com/handbook/customer-success/tam/onboarding/)/customer kickoffs
   - Kickoff
      - [Rubric](https://docs.google.com/forms/d/e/1FAIpQLSeZgqf6cU0rR0wvoOneGGh0jNaC0PXCzN5TEf_IBbBn80VxfQ/viewform)
          - Part of [Sales Quick Start](https://about.gitlab.com/handbook/sales/onboarding/)
      - TODO build scenario and roles
- Pre-sales
   - [Command of the Message](https://about.gitlab.com/handbook/sales/command-of-the-message/)
      - Part of [Sales Quick Start](https://about.gitlab.com/handbook/sales/onboarding/)
      - [Buyer info sheet](https://docs.google.com/document/d/1Zuy4z2YHZR0GXdQB_zexiknDKllgRab59wFWj3kpVnU/edit)
      - [Seller info sheet](https://docs.google.com/document/d/1jwLo3GYA81VNcXg7vHTRF7iMkF7YihV7a362yPtZx0o/edit)

